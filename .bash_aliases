# open alises
alias vi_alias='vi ~/.bash/.bash_aliases'
alias bash_update='source ~/.bashrc'

# python default version
alias python='python3'
alias pip='pip3'

# path directories
alias loc_prod='cd /mnt/d/production/'
alias loc_git='cd /mnt/d/workspace/'
